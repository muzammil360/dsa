# https://www.hackerrank.com/challenges/tree-inorder-traversal/problem

class Node:
    def __init__(self, info): 
        self.info = info  
        self.left = None  
        self.right = None 
        self.level = None 

    def __str__(self):
        return str(self.info) 

class BinarySearchTree:
    def __init__(self): 
        self.root = None

    def create(self, val):  
        if self.root == None:
            self.root = Node(val)
        else:
            current = self.root
         
            while True:
                if val < current.info:
                    if current.left:
                        current = current.left
                    else:
                        current.left = Node(val)
                        break
                elif val > current.info:
                    if current.right:
                        current = current.right
                    else:
                        current.right = Node(val)
                        break
                else:
                    break

"""
Node is defined as
self.left (the left child of the node)
self.right (the right child of the node)
self.info (the value of the node)
"""

def visitNode(node):
    '''
    visits a particular node given as input
    '''
    
    print(node, end = ' ')


def inOrder(root):
    if root is None: return 

    s = []
    s.append(root)
    c = root.left

    while (True):
        if c:
            s.append(c)
            c = c.left
        else:
            c = s.pop()
            visitNode(c)    # we visit it because it is a child node
            c = c.right
        
        if c is None and len(s)==0:
            break
    


tree = BinarySearchTree()
t = int(input())

arr = list(map(int, input().split()))

for i in range(t):
    tree.create(arr[i])

inOrder(tree.root)