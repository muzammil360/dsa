# https://www.hackerrank.com/challenges/tree-postorder-traversal/problem
class Node:
    def __init__(self, info): 
        self.info = info  
        self.left = None  
        self.right = None 
        self.level = None 

    def __str__(self):
        return str(self.info) 

class BinarySearchTree:
    def __init__(self): 
        self.root = None

    def create(self, val):  
        if self.root == None:
            self.root = Node(val)
        else:
            current = self.root
         
            while True:
                if val < current.info:
                    if current.left:
                        current = current.left
                    else:
                        current.left = Node(val)
                        break
                elif val > current.info:
                    if current.right:
                        current = current.right
                    else:
                        current.right = Node(val)
                        break
                else:
                    break

"""
Node is defined as
self.left (the left child of the node)
self.right (the right child of the node)
self.info (the value of the node)
"""
def visitNode(node):
    print(node, end = ' ')


def postOrder(root):
    if root is None: return 
    
    visit_map = {}
    isVisitable = lambda n: n and n not in visit_map
    isNotVisitable = lambda n: not isVisitable(n)
    
    s = []
    c = root
    
    while(True):
        if isVisitable(c):
            s.append(c)
            if isVisitable(c.right): s.append(c.right)
            c = c.left
        else:
            c = s.pop()
            if isNotVisitable(c.left) and isNotVisitable(c.right):
                visitNode(c)
                visit_map[c] = True
                
                
        if len(s)==0 and isNotVisitable(c) :
            break
    


tree = BinarySearchTree()
t = int(input())

arr = list(map(int, input().split()))

for i in range(t):
    tree.create(arr[i])

postOrder(tree.root)